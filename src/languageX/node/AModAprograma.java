/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AModAprograma extends PAprograma
{
    private PAprograma _left_;

    public AModAprograma()
    {
        // Constructor
    }

    public AModAprograma(
        @SuppressWarnings("hiding") PAprograma _left_)
    {
        // Constructor
        setLeft(_left_);

    }

    @Override
    public Object clone()
    {
        return new AModAprograma(
            cloneNode(this._left_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAModAprograma(this);
    }

    public PAprograma getLeft()
    {
        return this._left_;
    }

    public void setLeft(PAprograma node)
    {
        if(this._left_ != null)
        {
            this._left_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._left_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._left_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._left_ == child)
        {
            this._left_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._left_ == oldChild)
        {
            setLeft((PAprograma) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
