/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AVazioAdecCons1 extends PAdecCons1
{

    public AVazioAdecCons1()
    {
        // Constructor
    }

    @Override
    public Object clone()
    {
        return new AVazioAdecCons1();
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAVazioAdecCons1(this);
    }

    @Override
    public String toString()
    {
        return "";
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        throw new RuntimeException("Not a child.");
    }
}
