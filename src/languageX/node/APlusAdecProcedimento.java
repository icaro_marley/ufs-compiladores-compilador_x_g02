/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class APlusAdecProcedimento extends PAdecProcedimento
{
    private TParenR _parenR_;

    public APlusAdecProcedimento()
    {
        // Constructor
    }

    public APlusAdecProcedimento(
        @SuppressWarnings("hiding") TParenR _parenR_)
    {
        // Constructor
        setParenR(_parenR_);

    }

    @Override
    public Object clone()
    {
        return new APlusAdecProcedimento(
            cloneNode(this._parenR_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAPlusAdecProcedimento(this);
    }

    public TParenR getParenR()
    {
        return this._parenR_;
    }

    public void setParenR(TParenR node)
    {
        if(this._parenR_ != null)
        {
            this._parenR_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._parenR_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._parenR_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._parenR_ == child)
        {
            this._parenR_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._parenR_ == oldChild)
        {
            setParenR((TParenR) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
