/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class ATipoBaseAtipo extends PAtipo
{
    private PAtipoBase _atipoBase_;

    public ATipoBaseAtipo()
    {
        // Constructor
    }

    public ATipoBaseAtipo(
        @SuppressWarnings("hiding") PAtipoBase _atipoBase_)
    {
        // Constructor
        setAtipoBase(_atipoBase_);

    }

    @Override
    public Object clone()
    {
        return new ATipoBaseAtipo(
            cloneNode(this._atipoBase_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseATipoBaseAtipo(this);
    }

    public PAtipoBase getAtipoBase()
    {
        return this._atipoBase_;
    }

    public void setAtipoBase(PAtipoBase node)
    {
        if(this._atipoBase_ != null)
        {
            this._atipoBase_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._atipoBase_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._atipoBase_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._atipoBase_ == child)
        {
            this._atipoBase_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._atipoBase_ == oldChild)
        {
            setAtipoBase((PAtipoBase) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
