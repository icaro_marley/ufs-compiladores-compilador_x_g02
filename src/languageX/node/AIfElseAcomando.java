/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AIfElseAcomando extends PAcomando
{
    private PAexp _aexp_;
    private PAcomando _left_;
    private PAcomando _right_;

    public AIfElseAcomando()
    {
        // Constructor
    }

    public AIfElseAcomando(
        @SuppressWarnings("hiding") PAexp _aexp_,
        @SuppressWarnings("hiding") PAcomando _left_,
        @SuppressWarnings("hiding") PAcomando _right_)
    {
        // Constructor
        setAexp(_aexp_);

        setLeft(_left_);

        setRight(_right_);

    }

    @Override
    public Object clone()
    {
        return new AIfElseAcomando(
            cloneNode(this._aexp_),
            cloneNode(this._left_),
            cloneNode(this._right_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAIfElseAcomando(this);
    }

    public PAexp getAexp()
    {
        return this._aexp_;
    }

    public void setAexp(PAexp node)
    {
        if(this._aexp_ != null)
        {
            this._aexp_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._aexp_ = node;
    }

    public PAcomando getLeft()
    {
        return this._left_;
    }

    public void setLeft(PAcomando node)
    {
        if(this._left_ != null)
        {
            this._left_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._left_ = node;
    }

    public PAcomando getRight()
    {
        return this._right_;
    }

    public void setRight(PAcomando node)
    {
        if(this._right_ != null)
        {
            this._right_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._right_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._aexp_)
            + toString(this._left_)
            + toString(this._right_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._aexp_ == child)
        {
            this._aexp_ = null;
            return;
        }

        if(this._left_ == child)
        {
            this._left_ = null;
            return;
        }

        if(this._right_ == child)
        {
            this._right_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._aexp_ == oldChild)
        {
            setAexp((PAexp) newChild);
            return;
        }

        if(this._left_ == oldChild)
        {
            setLeft((PAcomando) newChild);
            return;
        }

        if(this._right_ == oldChild)
        {
            setRight((PAcomando) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
