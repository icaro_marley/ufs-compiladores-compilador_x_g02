/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AInicVarDec2 extends PVarDec2
{
    private TComma _comma_;
    private PInicializacao _inicializacao_;
    private PVarDec2 _varDec2_;

    public AInicVarDec2()
    {
        // Constructor
    }

    public AInicVarDec2(
        @SuppressWarnings("hiding") TComma _comma_,
        @SuppressWarnings("hiding") PInicializacao _inicializacao_,
        @SuppressWarnings("hiding") PVarDec2 _varDec2_)
    {
        // Constructor
        setComma(_comma_);

        setInicializacao(_inicializacao_);

        setVarDec2(_varDec2_);

    }

    @Override
    public Object clone()
    {
        return new AInicVarDec2(
            cloneNode(this._comma_),
            cloneNode(this._inicializacao_),
            cloneNode(this._varDec2_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAInicVarDec2(this);
    }

    public TComma getComma()
    {
        return this._comma_;
    }

    public void setComma(TComma node)
    {
        if(this._comma_ != null)
        {
            this._comma_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._comma_ = node;
    }

    public PInicializacao getInicializacao()
    {
        return this._inicializacao_;
    }

    public void setInicializacao(PInicializacao node)
    {
        if(this._inicializacao_ != null)
        {
            this._inicializacao_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._inicializacao_ = node;
    }

    public PVarDec2 getVarDec2()
    {
        return this._varDec2_;
    }

    public void setVarDec2(PVarDec2 node)
    {
        if(this._varDec2_ != null)
        {
            this._varDec2_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._varDec2_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._comma_)
            + toString(this._inicializacao_)
            + toString(this._varDec2_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._comma_ == child)
        {
            this._comma_ = null;
            return;
        }

        if(this._inicializacao_ == child)
        {
            this._inicializacao_ = null;
            return;
        }

        if(this._varDec2_ == child)
        {
            this._varDec2_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._comma_ == oldChild)
        {
            setComma((TComma) newChild);
            return;
        }

        if(this._inicializacao_ == oldChild)
        {
            setInicializacao((PInicializacao) newChild);
            return;
        }

        if(this._varDec2_ == oldChild)
        {
            setVarDec2((PVarDec2) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
