/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AConstDecBloco2 extends PBloco2
{
    private PConstDec _constDec_;

    public AConstDecBloco2()
    {
        // Constructor
    }

    public AConstDecBloco2(
        @SuppressWarnings("hiding") PConstDec _constDec_)
    {
        // Constructor
        setConstDec(_constDec_);

    }

    @Override
    public Object clone()
    {
        return new AConstDecBloco2(
            cloneNode(this._constDec_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAConstDecBloco2(this);
    }

    public PConstDec getConstDec()
    {
        return this._constDec_;
    }

    public void setConstDec(PConstDec node)
    {
        if(this._constDec_ != null)
        {
            this._constDec_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._constDec_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._constDec_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._constDec_ == child)
        {
            this._constDec_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._constDec_ == oldChild)
        {
            setConstDec((PConstDec) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
