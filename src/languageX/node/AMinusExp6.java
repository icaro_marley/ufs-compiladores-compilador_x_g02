/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AMinusExp6 extends PExp6
{
    private PExp6 _exp6_;
    private TMinus _minus_;
    private PExp7 _exp7_;

    public AMinusExp6()
    {
        // Constructor
    }

    public AMinusExp6(
        @SuppressWarnings("hiding") PExp6 _exp6_,
        @SuppressWarnings("hiding") TMinus _minus_,
        @SuppressWarnings("hiding") PExp7 _exp7_)
    {
        // Constructor
        setExp6(_exp6_);

        setMinus(_minus_);

        setExp7(_exp7_);

    }

    @Override
    public Object clone()
    {
        return new AMinusExp6(
            cloneNode(this._exp6_),
            cloneNode(this._minus_),
            cloneNode(this._exp7_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAMinusExp6(this);
    }

    public PExp6 getExp6()
    {
        return this._exp6_;
    }

    public void setExp6(PExp6 node)
    {
        if(this._exp6_ != null)
        {
            this._exp6_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._exp6_ = node;
    }

    public TMinus getMinus()
    {
        return this._minus_;
    }

    public void setMinus(TMinus node)
    {
        if(this._minus_ != null)
        {
            this._minus_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._minus_ = node;
    }

    public PExp7 getExp7()
    {
        return this._exp7_;
    }

    public void setExp7(PExp7 node)
    {
        if(this._exp7_ != null)
        {
            this._exp7_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._exp7_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._exp6_)
            + toString(this._minus_)
            + toString(this._exp7_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._exp6_ == child)
        {
            this._exp6_ = null;
            return;
        }

        if(this._minus_ == child)
        {
            this._minus_ = null;
            return;
        }

        if(this._exp7_ == child)
        {
            this._exp7_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._exp6_ == oldChild)
        {
            setExp6((PExp6) newChild);
            return;
        }

        if(this._minus_ == oldChild)
        {
            setMinus((TMinus) newChild);
            return;
        }

        if(this._exp7_ == oldChild)
        {
            setExp7((PExp7) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
