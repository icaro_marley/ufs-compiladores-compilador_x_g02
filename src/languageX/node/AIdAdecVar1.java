/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AIdAdecVar1 extends PAdecVar1
{
    private TId _id_;

    public AIdAdecVar1()
    {
        // Constructor
    }

    public AIdAdecVar1(
        @SuppressWarnings("hiding") TId _id_)
    {
        // Constructor
        setId(_id_);

    }

    @Override
    public Object clone()
    {
        return new AIdAdecVar1(
            cloneNode(this._id_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAIdAdecVar1(this);
    }

    public TId getId()
    {
        return this._id_;
    }

    public void setId(TId node)
    {
        if(this._id_ != null)
        {
            this._id_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._id_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._id_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._id_ == child)
        {
            this._id_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._id_ == oldChild)
        {
            setId((TId) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
