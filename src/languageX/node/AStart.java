/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.node;

import languageX.analysis.*;

@SuppressWarnings("nls")
public final class AStart extends PStart
{
    private PPrograma1 _programa1_;

    public AStart()
    {
        // Constructor
    }

    public AStart(
        @SuppressWarnings("hiding") PPrograma1 _programa1_)
    {
        // Constructor
        setPrograma1(_programa1_);

    }

    @Override
    public Object clone()
    {
        return new AStart(
            cloneNode(this._programa1_));
    }

    @Override
    public void apply(Switch sw)
    {
        ((Analysis) sw).caseAStart(this);
    }

    public PPrograma1 getPrograma1()
    {
        return this._programa1_;
    }

    public void setPrograma1(PPrograma1 node)
    {
        if(this._programa1_ != null)
        {
            this._programa1_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._programa1_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._programa1_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._programa1_ == child)
        {
            this._programa1_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._programa1_ == oldChild)
        {
            setPrograma1((PPrograma1) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
