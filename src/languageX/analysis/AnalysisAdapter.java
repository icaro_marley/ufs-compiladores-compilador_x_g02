/* This file was generated by SableCC (http://www.sablecc.org/). */

package languageX.analysis;

import java.util.*;
import languageX.node.*;

public class AnalysisAdapter implements Analysis
{
    private Hashtable<Node,Object> in;
    private Hashtable<Node,Object> out;

    @Override
    public Object getIn(Node node)
    {
        if(this.in == null)
        {
            return null;
        }

        return this.in.get(node);
    }

    @Override
    public void setIn(Node node, Object o)
    {
        if(this.in == null)
        {
            this.in = new Hashtable<Node,Object>(1);
        }

        if(o != null)
        {
            this.in.put(node, o);
        }
        else
        {
            this.in.remove(node);
        }
    }

    @Override
    public Object getOut(Node node)
    {
        if(this.out == null)
        {
            return null;
        }

        return this.out.get(node);
    }

    @Override
    public void setOut(Node node, Object o)
    {
        if(this.out == null)
        {
            this.out = new Hashtable<Node,Object>(1);
        }

        if(o != null)
        {
            this.out.put(node, o);
        }
        else
        {
            this.out.remove(node);
        }
    }

    @Override
    public void caseStart(Start node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAstart(AAstart node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADecVarAprograma(ADecVarAprograma node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADecConsAprograma(ADecConsAprograma node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADecProcedimentoAprograma(ADecProcedimentoAprograma node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADecFuncaoAprograma(ADecFuncaoAprograma node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAvazioAprograma(AAvazioAprograma node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADecVarAdecVar(ADecVarAdecVar node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAIdAdecVar1(AIdAdecVar1 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAInicAdecVar1(AInicAdecVar1 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAdecCons(AAdecCons node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAinicializacao(AAinicializacao node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAExpAinicializacao2(AExpAinicializacao2 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAArrayExtAinicializacao2(AArrayExtAinicializacao2 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAArrayCompAinicializacao2(AArrayCompAinicializacao2 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseATipoBaseAtipo(ATipoBaseAtipo node)
    {
        defaultCase(node);
    }

    @Override
    public void caseATipoArrayAtipo(ATipoArrayAtipo node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAIntAtipoBase(AIntAtipoBase node)
    {
        defaultCase(node);
    }

    @Override
    public void caseABoolAtipoBase(ABoolAtipoBase node)
    {
        defaultCase(node);
    }

    @Override
    public void caseARealAtipoBase(ARealAtipoBase node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAarrayExt(AAarrayExt node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAarrayComp(AAarrayComp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAArrayCompAarrayComp1(AArrayCompAarrayComp1 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAExpAarrayComp1(AExpAarrayComp1 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAdecProcedimento(AAdecProcedimento node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAdecFuncao(AAdecFuncao node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAVazioAparametros(AVazioAparametros node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAParametrosAparametros(AParametrosAparametros node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAparametro(AAparametro node)
    {
        defaultCase(node);
    }

    @Override
    public void caseATipoBaseAtipoParametro(ATipoBaseAtipoParametro node)
    {
        defaultCase(node);
    }

    @Override
    public void caseATipoArrayAtipoParametro(ATipoArrayAtipoParametro node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAbloco(AAbloco node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADecVarAbloco1(ADecVarAbloco1 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADecConsAbloco1(ADecConsAbloco1 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAComandoAbloco1(AComandoAbloco1 node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAIdAvar(AIdAvar node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAExpArrayAvar(AExpArrayAvar node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAblocoExp(AAblocoExp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAchamada(AAchamada node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAVazioAlistaExp(AVazioAlistaExp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAExpAlistaExp(AExpAlistaExp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseABlocoExpAexp(ABlocoExpAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAChamadaAexp(AChamadaAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAIfElseAexp(AIfElseAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAOrAexp(AOrAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAndAexp(AAndAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAEqualAexp(AEqualAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseALesserAexp(ALesserAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseABiggerAexp(ABiggerAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAPlusAexp(APlusAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAMinusAexp(AMinusAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAModAexp(AModAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAMulAexp(AMulAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseADivAexp(ADivAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseANegAexp(ANegAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseANotAexp(ANotAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAIntAexp(AIntAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseARealAexp(ARealAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseATrueAexp(ATrueAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAFalseAexp(AFalseAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAVarAexp(AVarAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAParAexp(AParAexp node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAWhileAcomando(AWhileAcomando node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAAtribAcomando(AAtribAcomando node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAChamadaAcomando(AChamadaAcomando node)
    {
        defaultCase(node);
    }

    @Override
    public void caseABlocoAcomando(ABlocoAcomando node)
    {
        defaultCase(node);
    }

    @Override
    public void caseAIfElseAcomando(AIfElseAcomando node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTTrue(TTrue node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTFalse(TFalse node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTInt(TInt node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTReal(TReal node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBool(TBool node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTCons(TCons node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTVar(TVar node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTWhile(TWhile node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTThen(TThen node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTIf(TIf node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTElse(TElse node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTProcedure(TProcedure node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTFunction(TFunction node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTPlus(TPlus node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTMinus(TMinus node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTDiv(TDiv node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTMul(TMul node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTAtrib(TAtrib node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTEqual(TEqual node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTAnd(TAnd node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTOr(TOr node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTLesser(TLesser node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBigger(TBigger node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTLessEq(TLessEq node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBiggerEq(TBiggerEq node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTPipe(TPipe node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTMod(TMod node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTDif(TDif node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTNot(TNot node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBracketL(TBracketL node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBracketR(TBracketR node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTParenL(TParenL node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTParenR(TParenR node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBraceL(TBraceL node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBraceR(TBraceR node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTSemi(TSemi node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTComma(TComma node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTBlank(TBlank node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTComment(TComment node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTId(TId node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTInteger(TInteger node)
    {
        defaultCase(node);
    }

    @Override
    public void caseTFloat(TFloat node)
    {
        defaultCase(node);
    }

    @Override
    public void caseEOF(EOF node)
    {
        defaultCase(node);
    }

    @Override
    public void caseInvalidToken(InvalidToken node)
    {
        defaultCase(node);
    }

    public void defaultCase(@SuppressWarnings("unused") Node node)
    {
        // do nothing
    }
}
