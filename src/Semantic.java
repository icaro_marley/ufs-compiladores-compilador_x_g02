/*
 * https://github.com/franciscocabral/br.ufs.compiladores/blob/master/PortugolCompiler/src/Main/mySemantic.java
 * http://www.dcs.gla.ac.uk/scripts/global/wim/blosxom.cgi/Gannet/Language/gannetc-compiler-sablecc.html
 * http://sablecc.sourceforge.net/thesis/thesis.html ast walkers
 * https://johntortugo.wordpress.com/2011/09/01/writing-a-compiler-with-sablecc/
 */

/*
orienta��es beatriz

-------------------------------------------------part1
impress�o de hash tablet toda vez que for feito 

se variavel foi passada como par�metro por refer�ncia de uma fun��o, e leitura alterar  inicializada para true 
vari�vel n�o inicializada warning, e setar inicialized como false 


///talvez seja necessario modificar, passar tipos dos parametros? em 
outAAdecFuncao
talvez seja necess�rio mudar inicialzia��o para receber o tipo arraycomp?


array comp � o recursivo 
array_compInicializa��o2 tem que retornar um array feito por composi��o
n�o sei como vou implementar array_comp (composi��o de arrays) for, aplicando expressao nos valores
(esperar at� exp estar protna)

resto de declara��o de constante e var dentro de bloco

-------------------------------------------------part2
tratar comandos e fun��o
n�o fazer desvio de fluxo, apenas checar tipo 
fazer print e scanf 

PAvar
FUN��O
parametros como outras variaveis na tabela e embaixo de fun�l�o, id.par1,id.par2
na chamada testar tipo na tabela
no escopo copiar valores para os parametros
comom udar o fluxo, no caminhar fazer visiar escopo
na declara��o de fun��o s� add como parametro
na chamada adicionar/remover escopo em has in e out
*/
import languageX.node.*;
import languageX.analysis.*;
import java.util.*;



import javax.swing.tree.DefaultMutableTreeNode;

public class Semantic extends DepthFirstAdapter{
	private Stack<Table> myStackTable;
	
	//auxiliares
	public  String get_value(String[] string_list,String[] tam_list,String[] index_list){//remover static
	       int base = Integer.parseInt(index_list[0]);
	       
	       if (tam_list.length == 1){
	           return string_list[base];
	       }

	       int multiplied = 1;
	       tam_list = Arrays.copyOfRange(tam_list, 1, tam_list.length);
	       index_list = Arrays.copyOfRange(index_list, 1, index_list.length);
	       
	       for (String element:tam_list){
	           multiplied = multiplied * Integer.parseInt(element);
	       }    
	       
	       string_list = Arrays.copyOfRange(string_list, base*multiplied, (base+1)*multiplied);   
	       return get_value(string_list,tam_list,index_list);
	}
	public  int get_index(String[] string_list,String[] tam_list,String[] index_list,int index){//remover static
	       int base = Integer.parseInt(index_list[0]);
	       
	       if (tam_list.length == 1){
	           return index + base;
	       }

	       int multiplied = 1;
	       tam_list = Arrays.copyOfRange(tam_list, 1, tam_list.length);
	       index_list = Arrays.copyOfRange(index_list, 1, index_list.length);
	       
	       for (String element:tam_list){
	           multiplied = multiplied * Integer.parseInt(element);
	       }    
	       int limiteInferior =  index + base*multiplied;
	       string_list = Arrays.copyOfRange(string_list, base*multiplied, (base+1)*multiplied);   
	       return get_index(string_list,tam_list,index_list,limiteInferior);
	}

	public void defaultOut(@SuppressWarnings("unused") Node node)
    {
        // remover espa�os em brancos das convers�es
    	if (node.value != null)
    		node.value = node.value.replaceAll("\\s+","");
    	if (node.type != null)
    		node.type = node.type.replaceAll("\\s+","");
    	if (node.id != null)
    		node.id = node.id.replaceAll("\\s+","");
    }

    public void inStart(Start node)
    {   //inicialzia tabela hash e pusha na pilha
    	Table myTable = new Table();
    	myStackTable = new Stack<Table>();
    	myStackTable.push(myTable);
    } 
    public void outAAstart(AAstart node)
    {	//remove a ultima hash da pilha
    	myStackTable.pop();
    }
    
    //declara��es 
    public void outAIdAdecVar1(AIdAdecVar1 node)
    {
        
        //achar tipo no pai.
        String type = ((ADecVarAdecVar)node.parent()).getAtipo().type;
        String id = node.getId().getText();
        node.id = id;
        node.type = type;
        node.value = "";
        node.initialized = false;
        //popa a tabela, introduz e fecha
        //testa se id j� existe
        Table tableAux = myStackTable.pop();
    	if(tableAux.isOnTable(id)){
    		try {//falta catar local da linha
				throw new Exception("Identificador Duplicado " + id);
						//+ "["+linha+","+pos+"]: "+key);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}
    	}else{

			String [] info = {type,"",String.valueOf(false)};
			tableAux.putInTable(id,info);
			String[] infoAux = tableAux.getFromTable(id);
			myStackTable.push(tableAux);	
		}
		defaultOut(node);
    }
    public void inADecProcedimentoAprograma(ADecProcedimentoAprograma node)//Procedimento Main, adiciona mais um escopo
    {
    	Table myTable = new Table();
    	myStackTable.push(myTable);

    } 
    public void outADecProcedimentoAprograma(ADecProcedimentoAprograma node)//tira do escopo
    {
    	myStackTable.pop();
    }
    public void outAAdecFuncao(AAdecFuncao node)//adiciona a fun��o na hash
    {
    	String id = node.getId().getText();
    	String type = node.getAtipo().type;
    	String arguments = node.getAparametros().toString();///talvez seja necessario modificar, passar tipos dos parametros?
    	String [] info = {type,arguments};
    	
    	node.id = id;
    	node.type = type;
    	
    	//checar se o tipo bate com o retorno!!!!
    	if (type != node.getAexp().type &&(!node.getAexp().type.equals(""))){
    		try {//falta catar local da linha
				throw new Exception("Tipo "+type+" da fun��o "+id+" n�o bate com seu retorno "+node.getAexp().type);
						//+ "["+linha+","+pos+"]: "+key);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}
    	}
    	
    	Table myTable = myStackTable.pop();
    	myTable.putInTable(id, info);
    	myStackTable.push(myTable);
    }           
    //inicializa��es
    public void outAExpAinicializacao2(AExpAinicializacao2 node)
    {
    	node.id = node.getAexp().id;
    	node.value = node.getAexp().value;
    	node.type  = node.getAexp().type;
        defaultOut(node);
    } 
    public void outAArrayExtAinicializacao2(AArrayExtAinicializacao2 node)
    {
    	node.id = node.getAarrayExt().id;
    	node.value = node.getAarrayExt().value;
    	node.type  = node.getAarrayExt().type;
        defaultOut(node);
    }
    public void outAArrayCompAinicializacao2(AArrayCompAinicializacao2 node)
    {
    	node.id = node.getAarrayComp().id;
    	node.value = node.getAarrayComp().value;
    	node.type  = node.getAarrayComp().type;
        defaultOut(node);
    }
    public void outAAinicializacao(AAinicializacao node)//checa tipos.usa getclass para decidir de quem � o tipo, cons ou variavel
    {
        String id = node.getId().getText();
        String value = node.getAinicializacao2().value;
        //tipo do valor
        String typeValue = node.getAinicializacao2().type;
        //tipo da declara��o
        
        
        ///teste quem � o pai para converter e pegar tipo       
        Node target = node.parent();
        //decvar ->decvar1
        //deccons
        String typeDeclaration = "";
        if (target instanceof AInicAdecVar1){
        	target = target.parent();
        	typeDeclaration = ((ADecVarAdecVar) target).getAtipo().type;
        }
        if (target instanceof AAdecCons){
        	typeDeclaration  = ((AAdecCons)target).getAtipo().type;
        }
        
        
        
        
        //checagem de tipos
        if (typeDeclaration.contains("real")){
        	typeValue = typeValue.replace("int", "real");
        }
        if (!typeValue.equals(typeDeclaration)&&(!typeDeclaration.equals("real")||!typeValue.equals("int"))){
    		try {//falta catar local da linha
				throw new Exception("Tipo declarado "+typeDeclaration +" diferente do atribuido " + typeValue);
						//+ "["+linha+","+pos+"]: "+key);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}
        }

    
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getAinicializacao2();
        if ((value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
        
        
        
        String type = typeValue;		
        //adicionar na hash, chegar se j� existe
        Table tableAux = myStackTable.pop();

    	if(tableAux.isOnTable(id)){
    		try {
				throw new Exception("Identificador Duplicado " + id);

			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}
    	}else{
    		String [] info = {type,value,String.valueOf(true)};
    		tableAux.putInTable(id,info);
    	}

    	myStackTable.push(tableAux);
    	defaultOut(node);
    	

    }
    public void outAAtribAcomando(AAtribAcomando node)//checa tipos e se uma const j� foi inicializada
    {
    	//valor a ser adicionado
        String value = node.getAexp().value;
        String typeValue = node.getAexp().type;
        //vari�vel
    	String id = node.getAvar().id;
        String varValue = node.getAvar().value;
        String typeVar = node.getAvar().type;
        
        
        if (typeVar.equals("")){
	 		try {//falta catar local da linha
					throw new Exception("Vari�vel/constante "+id+" n�o declarada");
							//+ "["+linha+","+pos+"]: "+key);
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
					System.exit(0);
				}
        }
        
        
        
        if (typeVar.contains("cons") && node.getAvar().initialized){//inicializada
	 		try {//falta catar local da linha
				throw new Exception("Constante "+id+" j� inicializada com "+ varValue);
						//+ "["+linha+","+pos+"]: "+key);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}
        	
        }
        	
        //checagem de tipos
        if (typeVar.contains("real")){
        	typeValue = typeValue.replace("int", "real");
        }
        if (!typeValue.equals(typeVar)&&(!typeVar.equals("real")||!typeValue.equals("int"))){
    		try {//falta catar local da linha
				throw new Exception("Tipo declarado "+typeVar +" diferente do atribu�do " + typeValue);
						//+ "["+linha+","+pos+"]: "+key);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}
        }
        		
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getAexp();
        if ((value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
        
        
        String type = typeVar;		

        //adicionar na hash.
    	Table tableAux = myStackTable.pop();
    	//atualizar entrada
    	node.getAvar().initialized = true;
    	String [] info = {type,value,String.valueOf(node.getAvar().initialized)};
    	tableAux.putInTable(id,info);
    	
    	
    	myStackTable.push(tableAux);
    	defaultOut(node);
    
    }
    //folhas e opera��es em exp
    //folhas de exp
    public void outAIntAexp(AIntAexp node)
    {
    	node.value = node.getInteger().toString();
    	node.type = "int";
    	defaultOut(node);
    }
    public void outARealAexp(ARealAexp node)
    {
    	node.value = node.getFloat().toString();
    	///System.out.println(node.value);
    	node.type = "real";
    	defaultOut(node);	
    }
    public void outATrueAexp(ATrueAexp node)
    {
    	node.value = node.getTrue().toString();
    	node.type = "bool";
    	defaultOut(node);
    }
    public void outAFalseAexp(AFalseAexp node)
    {
    	node.value = node.getFalse().toString();
    	node.type = "bool";
    	defaultOut(node);
    }
    public void outAVarAexp(AVarAexp node)//sobe com o valor e tipo da variavel
    {
    	PAvar target = node.getAvar();
    	node.type = target.type;
    	node.value = target.value;
    	node.id = target.id;
    	node.initialized = target.initialized;
    	defaultOut(node);

    }
    //C�LCULOS DE EXP
    public void outANegAexp(ANegAexp node)
    {
  
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getAexp();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
   
        PAexp target = node.getAexp();
        if (!target.type.equals("int") && !target.type.equals("real")){
    		try {
				throw new Exception("Opera��o de nega��o com valor n�o num�rico " + target.value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        else{
         	
        	if (target.value.equals("")){
        		node.value = "";
        		node.type = target.type;
        	}
        	else{
        		
        	
        	Double value = Double.parseDouble(target.value);
        	value = - value;
        	if (target.type.equals("int")){
        		int valueInt = value.intValue();
        		node.type = "int";
        	    node.value = String.valueOf(valueInt);
        	}
        	else{
        		node.type = "real";
        		node.value = String.valueOf(value);
        	}
        	}
        }
        defaultOut(node);
    }  
    public void outANotAexp(ANotAexp node)
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getAexp();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	PAexp target = node.getAexp();
    	
        if (!target.type.equals("bool")){
    		try {
				throw new Exception("Opera��o de not com valor n�o booleano " + target.value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        else{
          	if (target.value.equals("")){
        		node.value = "";
        		node.type = target.type;
        	}
          	else{
        	Boolean value = Boolean.valueOf(target.value);
        	value = ! value;
        	node.value = value.toString();
        	node.type = "bool";
          	}
        }
        defaultOut(node);
    }
    public void outAMinusAexp(AMinusAexp node)
    {
    	
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
    	
    	
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if ((!target1.type.equals("int") && !target1.type.equals("real")) ||
        	(!target2.type.equals("int") && !target2.type.equals("real")) ){
    		try {
				throw new Exception("Opera��o de subtra��o com valores n�o num�ricos " + target1.value + " - " + target2.value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        else{
        	
        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		if (target1.type.equals("real") || target2.type.equals("real")){//manter double
        			node.type = "real";
        		}
        		else{
        			node.type = "int";
        		}
        	}
        	else{
		        Double value = Double.parseDouble(target1.value) - Double.parseDouble(target2.value);
		        if (target1.type.equals("real") || target2.type.equals("real")){//manter double
		        	node.type = "real";
		        	node.value = String.valueOf(value);
		
		       	}
		        else{
		       		int valueInt = value.intValue();
		       		node.type = "int";
		       	    node.value = String.valueOf(valueInt);
		        	}
            	}
	        }
        defaultOut(node);
    }
    public void outADivAexp(ADivAexp node)
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
        
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if ((!target1.type.equals("int") && !target1.type.equals("real")) ||
        	(!target2.type.equals("int") && !target2.type.equals("real")) ){
    		try {
				throw new Exception("Opera��o de divis�o com valores n�o num�ricos " + target1.value + " / " + target2.value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        else{
        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		if (target1.type.equals("real") || target2.type.equals("real")){//manter double
        			node.type = "real";
        		}
        		else{
        			node.type = "int";
        		}
        	}
        	else{
        	
	        	Double value = Double.parseDouble(target1.value) / Double.parseDouble(target2.value);
	        	if (target1.type.equals("real") || target2.type.equals("real")){//manter double
	        		node.type = "real";
	        		node.value = String.valueOf(value);
	
	        	}
	        	else{
	        		int valueInt = value.intValue();
	        		node.type = "int";
	        	    node.value = String.valueOf(valueInt);
	        	}
        }
        }
        defaultOut(node);
    }
    public void outAMulAexp(AMulAexp node)
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
    	
    	
    	
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if ((!target1.type.equals("int") && !target1.type.equals("real")) ||
        	(!target2.type.equals("int") && !target2.type.equals("real")) ){
    		try {
				throw new Exception("Opera��o de multiplica��o com valores n�o num�ricos " + target1.value + " * " + target2.value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        else{
        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		if (target1.type.equals("real") || target2.type.equals("real")){//manter double
        			node.type = "real";
        		}
        		else{
        			node.type = "int";
        		}
        	}
        	else{

	        	Double value = Double.parseDouble(target1.value) * Double.parseDouble(target2.value);
	        	if (target1.type.equals("real") || target2.type.equals("real")){//manter double
	        		node.type = "real";
	        		node.value = String.valueOf(value);
	
	        	}
	        	else{
	        		int valueInt = value.intValue();
	        		node.type = "int";
	        	    node.value = String.valueOf(valueInt);
	        	}
        	}
        }
        defaultOut(node);
    }
    public void outAModAexp(AModAexp node)
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
        
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if ((!target1.type.equals("int") && !target1.type.equals("real")) ||
        	(!target2.type.equals("int") && !target2.type.equals("real")) ){
    		try {
				throw new Exception("Opera��o de resto com valores n�o num�ricos " + target1.value + " % " + target2.value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        else{

        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		if (target1.type.equals("real") || target2.type.equals("real")){//manter double
        			node.type = "real";
        		}
        		else{
        			node.type = "int";
        		}
        	}
        	else{
	        		
	        	Double value = Double.parseDouble(target1.value) % Double.parseDouble(target2.value);
	        	if (target1.type.equals("real") || target2.type.equals("real")){//manter double
	        		node.type = "real";
	        		node.value = String.valueOf(value);
	
	        	}
	        	else{
	        		int valueInt = value.intValue();
	        		node.type = "int";
	        	    node.value = String.valueOf(valueInt);
	        	}
        	}
        }
        defaultOut(node);
    }
    public void outAPlusAexp(APlusAexp node)
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
    	
    	
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight(); 	
        if ((!target1.type.equals("int") && !target1.type.equals("real")) ||
        	(!target2.type.equals("int") && !target2.type.equals("real")) ){
    		try {
				throw new Exception("Opera��o de adi��o com valores n�o num�ricos " + target1.value + " + " + target2.value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        else{
        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		if (target1.type.equals("real") || target2.type.equals("real")){//manter double
        			node.type = "real";
        		}
        		else{
        			node.type = "int";
        		}
        	}
        	else{
	        	Double value = Double.parseDouble(target1.value) + Double.parseDouble(target2.value);
	        	if (target1.type.equals("real") || target2.type.equals("real")){//manter double
	        		node.type = "real";
	        		node.value = String.valueOf(value);
	
	        	}
	        	else{
	        		int valueInt = value.intValue();
	        		node.type = "int";
	        	    node.value = String.valueOf(valueInt);
	        	}
        	}
        }
        defaultOut(node);
    }
    public void outABiggerAexp(ABiggerAexp node)
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if ((!target1.type.equals("int") && !target1.type.equals("real")) ||
            	(!target2.type.equals("int") && !target2.type.equals("real")) ){
        		try {
    				throw new Exception("Opera��o de compara��o com valores n�o num�ricos " + target1.value + " > " + target2.value);
    			} catch (Exception e) {
    				System.out.println(e.getMessage()); 
    				System.exit(0);
    			}	
        }
        else{
        	

        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		node.type = "bool";
        	}
        	else{
	
        	
	        	
	        	Boolean value = Double.parseDouble(target1.value) > Double.parseDouble(target2.value);
	        	node.value = value.toString();
	        	node.type = "bool";
        	}
        }
        defaultOut(node);
    }
    public void outALesserAexp(ALesserAexp node)
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if ((!target1.type.equals("int") && !target1.type.equals("real")) ||
            	(!target2.type.equals("int") && !target2.type.equals("real")) ){
        		try {
    				throw new Exception("Opera��o de compara��o com valores n�o num�ricos " + target1.value + " < " + target2.value);
    			} catch (Exception e) {
    				System.out.println(e.getMessage()); 
    				System.exit(0);
    			}	
        }
        else{
        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		node.type = "bool";
        	}
        	else{
        		
        	
        	Boolean value = Double.parseDouble(target1.value) < Double.parseDouble(target2.value);
        	node.value = value.toString();
        	node.type = "bool";
        }
        }
        defaultOut(node);
    }
    public void outAEqualAexp(AEqualAexp node)//implementado para bool int e real
    {
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
    	
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if (!target1.type.equals(target2.type)&&
        		((target1.type.equals("bool"))||
        		 (target2.type.equals("bool"))) ){
        		try {
    				throw new Exception("Opera��o de compara��o com tipos diferentes " + target1.value + " == " + target2.value);
    			} catch (Exception e) {
    				System.out.println(e.getMessage()); 
    				System.exit(0);
    			}	
        }
        else{
        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		node.type = "bool";
        	}
        	else{
        	
        	Boolean value = false;
        	if (target1.type.equals("real") || target1.type.equals("int"))
        		value = Double.parseDouble(target1.value) == Double.parseDouble(target2.value);
        	if (target1.type.equals("bool"))
        		value = Boolean.valueOf(target1.value) == Boolean.valueOf(target2.value);
        	node.value = value.toString();
        	node.type = "bool";
        }
        }
        defaultOut(node);
    }
    public void outAAndAexp(AAndAexp node)
    {
       	
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }

    	
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if (target1.type != "bool" ||
        	target2.type != "bool"){
        		try {
    				throw new Exception("Opera��o de compara��o com valores n�o num�ricos " + target1.value + " && " + target2.value);
    			} catch (Exception e) {
    				System.out.println(e.getMessage()); 
    				System.exit(0);
    			}	
        }
        else{

        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		node.type = "bool";
        	}
        	else{
	
        	Boolean value = Boolean.valueOf(target1.value)&& Boolean.valueOf(target2.value);
        	node.value = value.toString();
        	node.type = "bool";
        }
        }
        defaultOut(node);
    }
    public void outAOrAexp(AOrAexp node)
    {

    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        Node nodeTarget = node.getLeft();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
        
    	//se o valor dessa incializa��o for uma variavel n�o incializada? warning -> initialize true)
        nodeTarget = node.getRight();
        if ((nodeTarget.value == "") && (!nodeTarget.id.equals(""))){
        	//buscar na tabela se est� inicializada
        	Table tableAux1 = myStackTable.pop();
        	String[] info1 = tableAux1.getFromTable(nodeTarget.id);
        	Boolean initialized = Boolean.valueOf(info1[2]);
        	
        	if (!initialized){//se n�o estiver inicializada
        		//adicionar na hash initialize true
        		System.out.println("Warning: vari�vel/constante "+nodeTarget.id +" n�o inicializada");
        		String[] infoAux = {info1[0],info1[1],String.valueOf(true)};
            	tableAux1.putInTable(nodeTarget.id,infoAux);
        	}
        	
        	myStackTable.push(tableAux1);
        }
    	
    	PAexp target1 = node.getLeft();
    	PAexp target2 = node.getRight();
    	
        if (target1.type != "bool" || 
        	target2.type != "bool"){
        		try {
    				throw new Exception("Opera��o de compara��o com valores n�o num�ricos " + target1.value + " || " + target2.value);
    			} catch (Exception e) {
    				System.out.println(e.getMessage()); 
    				System.exit(0);
    			}	
        }
        else{
        	
        	if (target1.value.equals("")||target2.value.equals("")){
        		node.value = "";
        		node.type = "bool";
        	}
        	else{
        	Boolean value = Boolean.valueOf(target1.value)|| Boolean.valueOf(target2.value);
        	node.value = value.toString();
        	node.type = "bool";
        }
        }
        defaultOut(node);
    }
    public void outAIfElseAexp(AIfElseAexp node)//ternario
    {
        defaultOut(node);
        //se valor nao for true ou false de erro
        if (!(node.getLeft().value.equals("true")||node.getLeft().value.equals("false"))){
    		try {
				throw new Exception("Opera��o tern�ria com valor n�o booleano " + node.getLeft().value);
			} catch (Exception e) {
				System.out.println(e.getMessage()); 
				System.exit(0);
			}	
        }
        if (Boolean.valueOf(node.getLeft().value)){
        	node.type = node.getMid().type;
        	node.value = node.getMid().value;
        }
        else{
        	node.type = node.getRight().type;
        	node.value = node.getRight().value;
        }
        //node.getLeft()  //testar se � booleano
        //executar codigo normal
        //adicionar valor a exp com base no resultado.
    }
  
    //TIPOS BASE
    public void outATipoBaseAtipo(ATipoBaseAtipo node)
    {
    	node.value = node.getAtipoBase().value;
    	node.type  = node.getAtipoBase().type;
        defaultOut(node);
    }
    public void outATipoArrayAtipo(ATipoArrayAtipo node)//adiciona o [tamanho] ao type
    {
    	node.type = node.getAtipo().type + "[" + node.getAexp().value + "]";
        defaultOut(node);
    }   
    public void outAIntAtipoBase(AIntAtipoBase node)
    {
    	node.type = "int";
        defaultOut(node);
    }   
    public void outABoolAtipoBase(ABoolAtipoBase node)
    {
    	node.type = "bool";
        defaultOut(node);
    }    
    public void outARealAtipoBase(ARealAtipoBase node)
    {
    	node.type = "real";
        defaultOut(node);
    }
    public void outAIdAvar(AIdAvar node)//cata informa��es sobre uma vari�vel
    {
    	
    	node.id = node.getId().getText();
    	String id = node.id;
    	
        //procurar variavel na hash
        Table tableAux;
        Stack<Table> stackTableAux = new Stack<Table>();
    	while (true){
	    	if (myStackTable.empty())
	    	{
	    		try {
					throw new Exception("Vari�vel/constante n�o encontrada " + id);
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
					System.exit(0);
				}
	    	}
	    	tableAux = myStackTable.pop();
	    	stackTableAux.push(tableAux);
	    	
	    	if(tableAux.isOnTable(id)){
		    	String[] info = tableAux.getFromTable(id); 
		        //adiciona o valor da variavel
		        node.type = info[0];
		        node.value = info[1];
		        node.initialized = Boolean.valueOf(info[2]);
		      
		    	while (!stackTableAux.empty())
		    	{
		    		tableAux = stackTableAux.pop();
		    		myStackTable.push(tableAux);
		    	}
		    	
		    	break;
		    }
    	}
        defaultOut(node);
    }      
     

    //arrays    
    public void outAAarrayExt(AAarrayExt node)//n�o permite elementos de tipos diferentes
    {
    	node.type = node.getLeft().type;
    	node.value = node.getLeft().value;
    	int i = 0;
    	for (PAexp exp : node.getRight()){
    	//testar se o tipo se mantem.
    		if (!node.type.equals(exp.type)){
        		try {
    				throw new Exception("Array com valores de tipos diferentes" + node.value + " " + exp.value);
    			} catch (Exception e) {
    				System.out.println(e.getMessage()); 
    				System.exit(0);
    			}	
    		}
    		
    		else{
    			node.value = node.value+","+ node.getLeft().value;
    		}
    		
    		i = i + 1;
    	
    	}
    	node.type = node.type+"["+i+"]";
    	defaultOut(node);
    }
    public void outAExpAlistaExp(AExpAlistaExp node)//mesma implementa��o de arrayExt
    {
    	node.type = node.getLeft().type;
    	node.value = node.getLeft().value;
    	int i = 0;
    	for (PAexp exp : node.getRight()){
    	//testar se o tipo se mantem.
    		if (!node.type.equals(exp.type)){
        		try {
    				throw new Exception("Array com valores de tiops diferentes" + node.value + " " + exp.value);
    			} catch (Exception e) {
    				System.out.println(e.getMessage()); 
    				System.exit(0);
    			}	
    		}
    		
    		else{
    			node.value = node.value+","+ node.getLeft().value;
    		}
    		i = i + 1;
    	}
    	node.type = node.type+"["+i+"]";
        defaultOut(node);
    }
    public void outAVazioAlistaExp(AVazioAlistaExp node)//valor nulo, tipo do pai
    {
    	node.type = node.parent().type;//talvez esteja errado, ver quest�o dos []
        defaultOut(node);
    } 
    public void outAExpArrayAvar(AExpArrayAvar node)//variavel indexada no array, retornar valor dela.
    {
        node.id= node.getAvar().id;
        String id = node.id;
        //avan�ar indices pelas estruturas
        if (node.getAvar().value == null){
        	node.getAvar().value = node.getAexp().value;
        	node.value = node.getAvar().value;
        }
        else{
        	node.value = node.getAvar().value + "," + node.getAexp().value;
        }
      
        if ((node.parent().getClass().toString().contains("AVarAexp"))){
	        //procurar type na hash

        	Table tableAux;
	        Stack<Table> stackTableAux = new Stack<Table>();
	    	while (true){
		    	if (myStackTable.empty())
		    	{
		    		try {    
						throw new Exception("Vari�vel/constante n�o encontrada " + id);
					} catch (Exception e) {
						System.out.println(e.getMessage()); 
						System.exit(0);
					}
		    	}
		    	tableAux = myStackTable.pop();
		    	stackTableAux.push(tableAux);
		    	
		    	if(tableAux.isOnTable(id)){
			    	String[] info = tableAux.getFromTable(id); 
			        //adiciona o tipo da variavel
			        node.type = info[0]; 
			        
			        String posString = node.type;
			        if (!posString.matches(".*\\d+.*")){//n�o cont�m n�mero, n�o � um array
						try {
							throw new Exception("Identificador n�o � um array " + id);
						} catch (Exception e) {
							System.out.println(e.getMessage()); 
							System.exit(0);
						}   
			        }
			        //catar tamanhos no type
			        posString = posString.replace("[", " ").replace("]", " ");
			        //lista de indices para acessar
			        String[] tam_list = Arrays.copyOfRange(posString.split("\\s+"), 1, posString.split("\\s+").length);
			        String [] index_list = node.value.split(",");//�ndices
			        //checa se �ndices s�o n�meros v�lidos
			        int tamAux = 1;
			        int posAux = 1;
			        int i = 0;
			        
			        //checar se tem os mesmos tamanhos
			        if (tam_list.length != index_list.length)
			        {
						try {
							throw new Exception("Indices "+index_list.toString()+"com estrutura diferente do array " +tam_list.toString());
						} catch (Exception e) {
							System.out.println(e.getMessage()); 
							System.exit(0);
						}   
			        }
			        for (String pos:index_list){
			        	
			        	
				        try{
				        	   tamAux = Integer.parseInt(tam_list[i]);//tamanho do vetor
				        	   posAux = Integer.parseInt(pos);//posi��o do vetor
				        	   if (posAux < 0 || posAux >= tamAux){
							  		throw new NumberFormatException();
							  	}
				        }catch ( NumberFormatException e1){
							try {
								throw new Exception("�ndice inv�lido " + pos);
							} catch (Exception e) {
								System.out.println(e.getMessage()); 
								System.exit(0);
							}   
				        }
				        i = i + 1;
			        }
			 
			        
			        //testando se array foi inicializado
			        if (info[1] == null){
						try {
							throw new Exception("Array " + id +" n�o foi inicializado");
						} catch (Exception e) {
							System.out.println(e.getMessage()); 
							System.exit(0);
						}
			        }
			        
			        
			        int pos = -1;//encontrando posi��o certa
			        String[] string_list = info[1].split(",");
			        pos = get_index(string_list,tam_list,index_list,0);
			 	      
			        try{
			        	node.value =info[1].split(",")[pos];
			        }catch ( ArrayIndexOutOfBoundsException e1){
						try {
							throw new Exception("�ndice " + pos +"  fora dos limites no array "+ id);
						} catch (Exception e) {
							System.out.println(e.getMessage()); 
							System.exit(0);
						}   
						
					node.type = node.type.replaceAll("\\[\\d+\\]", "");//remover todos [numero] com regex
			        }
			       
			        
			     
			    	while (!stackTableAux.empty())
			    	{
			    		tableAux = stackTableAux.pop();
			    		myStackTable.push(tableAux);
			    	}
			    	
			    	break;
			    }
	    	}
	

        }
        defaultOut(node);   
    }

    
    ///////////////////////////////////////////////////////////////////////////////////////////

    //depender de fun��o, funciona tamb�m com desvio de escopo
    public void outAAarrayComp(AAarrayComp node)
    {
        defaultOut(node);
    }
    public void outAArrayCompAarrayComp1(AArrayCompAarrayComp1 node)
    {
        defaultOut(node);
    }

    //bloco, VER QUAL � A DIFEREN�A DE CADA UM
    public void inAAbloco(AAbloco node)//cria novo escopo
    {
    	Table myTable = new Table();
    	myStackTable.push(myTable);
    }
    public void outAAbloco(AAbloco node)//remove novo escopo
    {
    	myStackTable.pop();
    }
    public void inAAblocoExp(AAblocoExp node)//cria novo escopo
    {
    	Table myTable = new Table();
    	myStackTable.push(myTable);
    } 
    public void outAAblocoExp(AAblocoExp node)//remove novo escopo
    {
    	myStackTable.pop();
    }
    public void inABlocoExpAexp(ABlocoExpAexp node)//cria novo escopo
    {
    	Table myTable = new Table();
    	myStackTable.push(myTable);
    }
    public void outABlocoExpAexp(ABlocoExpAexp node)//remove novo escopo
    {
    	myStackTable.pop();
    }
    public void inABlocoAcomando(ABlocoAcomando node)//cria novo escopo
    {
    	Table myTable = new Table();
    	myStackTable.push(myTable);
    }
    public void outABlocoAcomando(ABlocoAcomando node)//remove novo escopo
    {
    	myStackTable.pop();
    }

    public void outAComandoAbloco1(AComandoAbloco1 node)
    {
        defaultOut(node);
    }
      
    //controle e chamada
    public void inAAchamada(AAchamada node)//mudar controle pra fun��o? adicionar escopo?
    {
        defaultIn(node);
    } 
    public void outAAchamada(AAchamada node)//remover escopo da fun��o
    {
      	myStackTable.pop();
    }
    //ins e outs daqui tamb�m. ver qual � a diferen�a de cada chamada
    public void outAChamadaAexp(AChamadaAexp node)
    {
        defaultOut(node);
    }
    public void outAChamadaAcomando(AChamadaAcomando node)
    {
        defaultOut(node);
    }
    
    //pensar como implementar isso aqui
    public void outAParametrosAparametros(AParametrosAparametros node)
    {
        defaultOut(node);
    }
    
    public void outAAparametro(AAparametro node)
    {
        defaultOut(node);
    }
    
    public void outATipoBaseAtipoParametro(ATipoBaseAtipoParametro node)
    {
        defaultOut(node);
    }
    
    public void outATipoArrayAtipoParametro(ATipoArrayAtipoParametro node)
    {
        defaultOut(node);
    }
 
    //controle

    public void outAWhileAcomando(AWhileAcomando node)
    {
        defaultOut(node);
      //testar se � booleano
        //executar codigo normal
    }

    public void outAIfElseAcomando(AIfElseAcomando node)
    {
        defaultOut(node);
      //testar se � booleano
        //executar codigo normal
    }
    

}
