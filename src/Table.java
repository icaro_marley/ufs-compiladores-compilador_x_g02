import java.util.Hashtable;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

public class Table {

	public Map<String, String[]> myTable = new HashMap<String, String[]>();
	//id : [type,value...]
	//Fun��es auxiliares
    public String getTipo(String key){
		String tipo;
		if(isOnTable(key)){
			return getFromTable(key)[1];
		}
		return null;
    }
    
    
	public String[] remFromTable(String key){
		return this.myTable.remove(key);
	}
	
	public String[] getFromTable(String key){
		return this.myTable.get(key);
	}
	
	public String[] putInTable(String key, String[] info){
		if(info != null){
			return this.myTable.put(key, info);
		}
		return null;
	}
	
	public boolean isOnTable(String key){
		return this.myTable.containsKey(key);
	}
}
