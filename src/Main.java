import languageX.parser.* ; 
import languageX.lexer.* ; 
import languageX.node.* ; 
import java.io.* ; 


public class Main { 
		//usar mesmo algoritmo para um "set_value" e usar em outAExpArrayAvar

   public static void main(String[] args) { 
      if (args.length > 0) { 
         try { 
            /* Form our AST */ 
            Lexer lexer1 = new Lexer (
            		new PushbackReader(new FileReader(args[0]),1024));
            Lexer lexer2 = new Lexer (
            		new PushbackReader(new FileReader(args[0]),1024));
           
            
            //Impressao de tokens:
            Token token;
            String token_str = " ";
            System.out.println("1-TOKENS:");
             while (true){
  	            token = lexer1.next();
	            token_str = token.getText();
	            if (token_str == "")
	            	break;
	            System.out.println(token_str + ":::" + token.getClass().getSimpleName());
            }
             System.out.println("\n"); 
             System.out.println("2-PRINTING TREE:"); 
            
            
            //ana sintatica	
            
            Parser parser = new Parser(lexer2); 
            Start ast = parser.parse() ;
            //ast.apply(new ASTPrinter());
            //ast.apply(new ASTDisplay());
            System.out.println("3-SEMANTIC ERROS/WARNINGS:"); 
            ast.apply(new Semantic());
            
            //semantica  try catch com impressao de erros
            //classe semantica fora
            //instancia aqui e da o apply assim como a class ast
            //comparar asts com a semantica de chico
            /*
    comp:  herdar classes de passeio e fazer a pr�pria default in e out 
    m�todos para cada um dos nos. case rege o passeio ,  
    ler eric gama padr�es de projeto. AminusExp exp que levou a minus na �rvore abstrata.
     dephtfirst. para a tabela de s�mbolos usar uma varia��o da hashmap.
      ver slides. sl 34 hash de pilha de hastes.
       passagem nas �rvores, 
    tabela de s�mbolos (pilha de hashes)
             */
           
             
         } 
         catch (LexerException le) {
             System.out.println("\n Lexer Error Message: \n" + le.getMessage());
         } catch (ParserException pe){
             System.out.println("\n Parser Error Message: \n " + pe.getMessage());
         } catch (Exception e) {
             throw new RuntimeException("\n Semantic Error Message: " + e.getMessage());
         }
      } else { 
         System.err.println("usage: java simpleAdder inputFile"); 
         System.exit(1); 
      } 
   } 
}